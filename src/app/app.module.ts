import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
// Environment
import {environment} from '../environments/environment';
// AngularFire
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
// Angular Material 2
import {MdButtonModule,
        MdChipsModule,
        MdCoreModule,
        MdDialogModule,
        MdExpansionModule,
        MdGridListModule,
        MdIconModule,
        MdInputModule,
        MdListModule,
        MdMenuModule,
        MdProgressBarModule,
        MdSelectModule,
        MdTabsModule,
        MdToolbarModule,
        OverlayModule} from '@angular/material';
// App component
import {AppComponent} from './app.component';
// Router
import {AppRouterModule} from './routing/app.routes';
// Components
import {HomeComponent,
        CommandsComponent,
        DeviceInfoComponent,
        SensorsComponent,
        SettingsComponent,
        MetadataComponent} from './home/home.component';

import {ToolbarComponent} from './shared/toolbar/toolbar.component';
import {DialogComponent} from './shared/dialog/dialog.component';
// Form attr component.
import {FormComponent} from './shared/form/form.component';
import {FormDetailComponent} from './shared/form/form-detail.component';
// Services
import {FormService} from './shared/form/form.service';
// External
import 'hammerjs';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ToolbarComponent,
    DialogComponent,
    CommandsComponent,
    DeviceInfoComponent,
    SensorsComponent,
    SettingsComponent,
    MetadataComponent,
    FormComponent,
    FormDetailComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    RouterModule,
    MdButtonModule,
    MdChipsModule,
    MdCoreModule,
    MdDialogModule,
    MdExpansionModule,
    MdGridListModule,
    MdIconModule,
    MdInputModule,
    MdListModule,
    MdMenuModule,
    MdProgressBarModule,
    MdSelectModule,
    MdTabsModule,
    MdToolbarModule,
    OverlayModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot(
      AppRouterModule,
      {enableTracing: false}
    )
  ],
  entryComponents: [DialogComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [FormService],
  bootstrap: [AppComponent]
})
export class AppModule {}

import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
   public navLinks: any[] = [
    { id: 1, content: '/device-info', label: 'Device Info'},
    { id: 2, content: '/sensors', label: 'Sensors'},
    { id: 3, content: '/settings', label: 'Settings'},
    { id: 4, content: '/commands', label: 'Commands'},
    { id: 5, content: '/metadata', label: 'Metadata'}
  ];
}

const template = '<app-form-attr [nameTab]="title"></app-form-attr>';

@Component({
  selector: 'app-device-info',
  template: template,
  styleUrls: ['./home.component.css']
})
export class DeviceInfoComponent { title = 'Device Info'; }

@Component({
  selector: 'app-sensors',
  template: template,
  styleUrls: ['./home.component.css']
})
export class SensorsComponent { title = 'Sensors'; }

@Component({
  selector: 'app-settings',
  template: template,
  styleUrls: ['./home.component.css']
})
export class SettingsComponent { title = 'Settings'; }

@Component({
  selector: 'app-commands',
  template: template,
  styleUrls: ['./home.component.css']
})
export class CommandsComponent { title = 'Commands'; }

@Component({
  selector: 'app-metadata',
  template: template,
  styleUrls: ['./home.component.css']
})
export class MetadataComponent { title = 'Metadata'; }

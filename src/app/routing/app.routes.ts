import { Routes, RouterModule } from '@angular/router';
import { CommandsComponent,
         DeviceInfoComponent,
         SensorsComponent,
         SettingsComponent,
         MetadataComponent } from '../home/home.component';

export const AppRouterModule: Routes = [
  {path: 'device-info', component: DeviceInfoComponent},
  {path: 'sensors', component: SensorsComponent},
  {path: 'settings', component: SettingsComponent },
  {path: 'commands', component: CommandsComponent},
  {path: 'metadata', component: MetadataComponent},
  {path: '',
    redirectTo: '/metadata',
    pathMatch: 'full'
  }
];

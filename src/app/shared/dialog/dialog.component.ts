import { Component } from '@angular/core';
import { MdDialogRef } from '@angular/material';
// Model
import { Message } from './dialog.model';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent {

  Msg = new Message('');

  constructor(public dialogRef: MdDialogRef<DialogComponent>) {}
}

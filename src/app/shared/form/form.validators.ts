import { FormControl } from '@angular/forms';

// Valid empty field.
export function validEmptyField (input: FormControl) {
  const valueIsNotEmpty = input.value !== ' ';
  return valueIsNotEmpty ? null : { validEmtpy: true };
}

// Valid Range Min - Max.
export function validMinMaxRange (input: FormControl) {
  const ranges = input.value >= 5 && input.value <= 10;
  return ranges ? null : { range: ranges };
}

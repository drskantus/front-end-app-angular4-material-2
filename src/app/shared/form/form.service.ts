import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';

@Injectable()
export class FormService {

  dbAttributes: FirebaseListObservable<any>;
  dbAttributesObj: FirebaseObjectObservable<any>;
  database;
  newRef;
  keyRef;
  isDuplicate;

  constructor(public db: AngularFireDatabase) {
    this.database = db;
    this.dbAttributes = this.database.list('/user/list/');
    this.dbAttributesObj = this.database.object('/user/list/', { preserveSnapshot: true });
  }
  // GET ALL
  getAttrsList() {
    return this.dbAttributes;
  }
  // SAVE
  saveAttribute(form) {
    this.newRef = this.dbAttributes.push(form);
    this.dbAttributes.update(this.newRef.key, { id: this.newRef.key }).then(function(){
      console.log('response: ', 'Attribute added successfully');
    });
  }
  // DELETE
  deleteAttribute(id) {
    this.dbAttributes.remove(id).then(function(){
      console.log('response: ', 'Attribute was deleted successfully.');
    });
  }
  // UPDATE
  updateAttribute(id, obj) {
    this.dbAttributes.update(id, obj).then(function(){
      console.log('response: ', 'Attribute was updated successfully.');
    });
  }
  // VALID duplicate attr.
  duplicateAttribute(name) {
    this.isDuplicate = false;
    this.dbAttributesObj.subscribe(snapshots => {
      snapshots.forEach(snapshot => {
        if (snapshot.val()) {
          if (snapshot.val().name === name) {
            this.isDuplicate = true;
          }
        }
      });
    });
    return this.isDuplicate;
  }
}


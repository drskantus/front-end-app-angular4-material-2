import { Component, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MdDialog } from '@angular/material';
import { DialogComponent } from '../../shared/dialog/dialog.component';
// #Models
import { Attribute, deviceResourceTypeList, formatList, dataType, availableChips } from './form-model';
// #Services
import { FormService } from './form.service';
// #Validators
import { validEmptyField, validMinMaxRange } from './form.validators';

@Component({
  selector: 'app-form-detail',
  templateUrl: './form-detail.component.html',
  styleUrls: ['./form.component.css']
})
export class FormDetailComponent implements OnChanges {
  @Input() attr: Attribute;
  @Input() savedStatus: boolean;
  @Output() validSaveForm: EventEmitter<any> = new EventEmitter();

  selectedOption: string;
  dialogRef;
  attributesForm: FormGroup;
  objAttrs;

  deviceResourceTypeList = deviceResourceTypeList
  formatList = formatList;
  dataType = dataType;
  availableColors = availableChips;
  arrayValuesList = [];
  flagNewAttr = false;

  dataTypeObj = { value: null, disabled: true };
  enumStatus;
  duplicateAttrs;

  constructor(
    private fb: FormBuilder,
    public dialog: MdDialog,
    public _formService: FormService) {
      this.initializeForm();
  }

  // Create a 'attributesForm' for each attributes list.
  initializeForm() {
    this.savedStatus = false;
    this.attributesForm = this.fb.group({
      name: new FormControl({ value: null, disabled: false }, [Validators.required, validEmptyField]),
      description: new FormControl({ value: null, disabled: false }),
      defaultValue: new FormControl({ value: null, disabled: false }),
      dataType: new FormControl({ value: null, disabled: false }),
      format: new FormControl({ value: null, disabled: false }),
      enumerations: new FormControl({ value: [], disabled: false }),
      // Ranges.
      rangeMin: new FormControl(null),
      rangeMax: new FormControl(null),
      uom: new FormControl(null),
      precision: new FormControl(null),
      accuracy: new FormControl(null)
    });

    // Notify value changes to global 'SAVE' button.
    this.attributesForm.valueChanges.subscribe((data) => {
      this.validSaveForm.emit(this.attributesForm.valid);
    });

    // Validation for unique name attributes.
    this.attributesForm.get('name').valueChanges.subscribe((name) => {
      this.duplicateAttrs = this._formService.duplicateAttribute(name);
      if (this.duplicateAttrs) {
        this.attributesForm.get('name').setErrors({ validEmtpy: this.duplicateAttrs });
        return false;
      }
      this.attributesForm.get('name').clearAsyncValidators();
    });

    // Validation Default Value is disabled - Format is disabled.
    this.attributesForm.get('dataType').valueChanges.subscribe((dataType) => {
      if (dataType === 1) {
        this.dataTypeObj = { value: null, disabled: true };
      } else {
        this.dataTypeObj = { value: null, disabled: false };
      }
      this.attributesForm.get('defaultValue').reset({ value: this.dataTypeObj.value, disabled: this.dataTypeObj.disabled });
      this.attributesForm.get('format').reset({ value: this.dataTypeObj.value, disabled: this.dataTypeObj.disabled });
    });

    // Validation When “none” is selected it activates an enumerated list of strings.
    this.attributesForm.get('format').valueChanges.subscribe((format) => {
      if (format === 0) {
        this.enumStatus = false;
      } else {
        this.enumStatus = true;
      }
      this.arrayValuesList = [];
      this.attributesForm.get('rangeMin').reset();
      this.attributesForm.get('rangeMax').reset();
      this.attributesForm.get('uom').reset();
      this.attributesForm.get('precision').reset();
      this.attributesForm.get('accuracy').reset();
    });

  }

  // Detect all changes when a user press click in selected element.
  ngOnChanges() {
    // Save form in db.
    if (this.savedStatus) {
      this.onSubmit(this.attributesForm);
      return false;
    }
    // Validator new attr.
    if (!this.attr.id) {
      if (!this.flagNewAttr) {
        this.flagNewAttr = true;
        this.arrayValuesList = [];
        this.attributesForm.reset();
      }
    } else {
        this.attributesForm.reset({
        id: this.attr.id,
        name: this.attr.name,
        description: this.attr.description,
        defaultValue: this.attr.defaultValue,
        dataType: this.attr.dataType,
        format: this.attr.format,
        enumerations: [],
        // Ranges.
        rangeMin: this.attr.rangeMin,
        rangeMax: this.attr.rangeMax,
        uom: this.attr.uom,
        precision: this.attr.precision,
        accuracy: this.attr.accuracy
      });
      this.flagNewAttr = false;
      this.arrayValuesList = this.attr.enumerations || [];
    }
  }

  // Submit the form when is valid.
  onSubmit(form: FormGroup) {
    // Valid form
    if (!form.valid) { return false; }
    form.value.enumerations = this.arrayValuesList;
    this._formService.saveAttribute(form.value);
    // Clean array chips.
    this.arrayValuesList = [];
    // Reset all the form.
    this.attributesForm.reset();
    this.attributesForm.controls['name'].clearAsyncValidators();
    this.attributesForm.controls['name'].clearValidators();
  }

  // Add the value to enumerations array.
  addValuesToEnum() {
    if (!this.attributesForm.get('enumerations').value || this.attributesForm.get('enumerations').value.length === 0) {
      return false;
    }
    this.arrayValuesList.push(this.attributesForm.get('enumerations').value);
    this.attributesForm.get('enumerations').reset();
  }

  // Function DELETE attribute.
  deleteAttribute() {
    this.dialogRef = this.dialog.open(DialogComponent);
    this.dialogRef.afterClosed().subscribe(result => {
      this.selectedOption = result;
      if (result === 'true') {
        this._formService.deleteAttribute(this.attr.id);
      }
    });
  }
}

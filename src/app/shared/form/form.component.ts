import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
// Models
import { Attribute } from './form-model';
// #Services
import { FormService } from './form.service';

@Component({
  selector: 'app-form-attr',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormComponent {
  @Input() nameTab;
  // Attribute model.
  selectedAttr = new Attribute();
  // Firebase database.
  dbAttributes;
  invalidSaveButton = {
    invalid: true,
    save: false
  };

  constructor(public _formService: FormService) {
    // Get array list from service.
    this.dbAttributes = _formService.getAttrsList();
  }
  // Select the current attribute.
  selectAttribute(attribute: Attribute, status) {
    // If select a new attr send an empty object.
    (!attribute) ? this.selectedAttr = new Attribute() : this.selectedAttr = attribute;
  }

  // Validator for global 'SAVE' button.
  getValidSaveForm(evt) {
    (evt) ? this.invalidSaveButton.invalid = false : this.invalidSaveButton.invalid = true;
  }

  submitFormAttr() {
    // @input detect viability to save form.
    this.invalidSaveButton.save = true;
    this.invalidSaveButton.invalid = true;
    setTimeout(() => {
      this.invalidSaveButton.save = false;
    }, 200);
  }
}

// Class Attributeç
export class Attribute {
  id: any;
  name: string;
  description: string;
  defaultValue: any;
  dataType: any;
  format: any;
  enumerations = [];
  rangeMin: any;
  rangeMax: any;
  uom: any;
  precision: any;
  accuracy: any;
  isValid?: any;
}

export const deviceResourceTypeList = [
  {value: 0, viewValue: 'Default Value'}
];

export const dataType = [
  {value: 0, viewValue: 'STRING'},
  {value: 1, viewValue: 'OBJECT'}
];

export const formatList = [
  {value: 0, viewValue: 'NONE'},
  {value: 1, viewValue: 'NUMBER'},
  {value: 2, viewValue: 'BOOLEAN'},
  {value: 3, viewValue: 'DATE-TIME'},
  {value: 4, viewValue: 'CDATA'},
  {value: 5, viewValue: 'URI'}
];

export const availableChips = [
  { name: 'none', color: '' },
  { name: 'Primary', color: 'primary' },
  { name: 'Accent', color: 'accent' },
  { name: 'Warn', color: 'warn' }
];
